{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# (PART) Data Modeling via moderndive {-} \n",
    "\n",
    "# Basic Regression {#regression}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "chap = 6\n",
    "lc = 0\n",
    "rq = 0\n",
    "print(\"(LC\", chap, \".\", (lc), \")\")\n",
    "print(\"(RQ\", chap, \".\", (rq), \")\")\n",
    "lc+= 1\n",
    "rq += 1\n",
    "import math\n",
    "import scipy as sp  \n",
    "import pandas as pd  \n",
    "import plotnine as pn \n",
    "import statsmodels.api as sm\n",
    "import statsmodels.formula.api as smf\n",
    "sp.random.seed(76)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we are equipped with data visualization skills from Chapter \\@ref(viz), an understanding of the \"tidy\" data format from Chapter \\@ref(tidy), and data wrangling skills from Chapter \\@ref(wrangling), we now proceed with data modeling. The fundamental premise of data modeling is *to make explicit the relationship* between:\n",
    "\n",
    "* an outcome variable $y$, also called a dependent variable and \n",
    "* an explanatory/predictor variable $x$, also called an independent variable or covariate. \n",
    "\n",
    "Another way to state this is using mathematical terminology: we will model the outcome variable $y$ *as a function* of the explanatory/predictor variable $x$. Why do we have two different labels, explanatory and predictor, for the variable $x$? That's because roughly speaking data modeling can be used for two purposes:\n",
    "\n",
    "1. **Modeling for prediction**: You want to predict an outcome variable $y$ based on the information contained in a set of predictor variables. You don't care so much about understanding how all the variables relate and interact, but so long as you can make good predictions about $y$, you're fine. For example, if we know many individuals' risk factors for lung cancer, such as smoking habits and age, can we predict whether or not they will develop lung cancer? Here we wouldn't care so much about distinguishing the degree to which the different risk factors contribute to lung cancer, but instead only on whether or not they could be put together to make reliable predictions.\n",
    "1. **Modeling for explanation**: You want to explicitly describe the relationship between an outcome variable $y$ and a set of explanatory variables, determine the significance of any found relationships, and have measures summarizing these. Continuing our example from above, we would now be interested in describing the individual effects of the different risk factors and quantifying the magnitude of these effects. One reason could be to design an intervention to reduce lung cancer cases in a population, such as targeting smokers of a specific age group with an advertisement for smoking cessation programs. In this book, we'll focus more on this latter purpose.\n",
    "\n",
    "Data modeling is used in a wide variety of fields, including statistical inference, causal inference, artificial intelligence, and machine learning. There are many techniques for data modeling, such as tree-based models, neural networks and deep learning, and supervised learning. In this chapter, we'll focus on one particular technique: *linear regression*, one of the most commonly-used and easy-to-understand approaches to modeling. Recall our discussion in Subsection \\@ref(exploredataframes) on numerical and categorical variables. Linear regression involves:\n",
    "\n",
    "* an outcome variable $y$ that is *numerical* and \n",
    "* explanatory variables $\\vec{x}$ that are either *numerical* or *categorical*.\n",
    "\n",
    "With linear regression there is always only one numerical outcome variable $y$ but we have choices on both the number and the type of explanatory variables $\\vec{x}$ to use. We're going to cover the following regression scenarios:\n",
    "\n",
    "* In this current chapter on basic regression, we'll always have only one explanatory variable.\n",
    "    + In Section \\@ref(model1), this explanatory variable will be a single numerical explanatory variable $x$. This scenario is known as *simple linear regression*. \n",
    "    + In Section \\@ref(model2), this explanatory variable will be a categorical explanatory variable $x$.\n",
    "* In the next chapter, Chapter \\@ref(multiple-regression) on *multiple regression*, we'll have more than one explanatory variable:\n",
    "    + We'll focus on two numerical explanatory variables $x_1$ and $x_2$ in Section \\@ref(model3). This can be denoted as $\\vec{x}$ as well since we have more than one explanatory variable.\n",
    "    + We'll use one numerical and one categorical explanatory variable in Section \\@ref(model3). We'll also introduce *interaction models* here; there, the effect of one explanatory variable depends on the value of another. \n",
    "\n",
    "We'll study all four of these regression scenarios using real data, all easily accessible via Python packages! <!--We will also discuss the concept of *correlation* and how it is frequently incorrectly used to imply *causation*.-->\n",
    "\n",
    "If you have not already, install the gapminder data using pip"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "pip install gapminder"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from gapminder import gapminder\n",
    "gapminder.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## One numerical explanatory variable {#model1}\n",
    "\n",
    "Why do some professors and instructors at universities and colleges get high teaching evaluations from students while others don't? What factors can explain these differences? Are there biases? These are questions that are of interest to university/college administrators, as teaching evaluations are among the many criteria considered in determining which professors and instructors should get promotions. Researchers at the University of Texas in Austin, Texas (UT Austin) tried to answer this question: what factors can explain differences in instructor's teaching evaluation scores? To this end, they collected information on $n = 463$ instructors. A full description of the study can be found at [openintro.org](https://www.openintro.org/stat/data/?data=evals).\n",
    "\n",
    "We'll keep things simple for now and try to explain differences in instructor evaluation scores as a function of one numerical variable: their \"beauty score.\" The specifics on how this score was calculated will be described shortly. \n",
    "\n",
    "Could it be that instructors with higher beauty scores also have higher teaching evaluations? Could it be instead that instructors with higher beauty scores tend to have lower teaching evaluations? Or could it be there is no relationship between beauty score and teaching evaluations? \n",
    "\n",
    "We'll achieve ways to address these questions by modeling the relationship between these two variables with a particular kind of linear regression called *simple linear regression*. Simple linear regression is the most basic form of linear regression. With it we have\n",
    "\n",
    "1. A numerical outcome variable $y$. In this case, their teaching score.\n",
    "1. A single numerical explanatory variable $x$. In this case, their beauty score."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exploratory data analysis {#model1EDA}\n",
    "\n",
    "A crucial step before doing any kind of modeling or analysis is performing an *exploratory data analysis*, or EDA, of all our data. Exploratory data analysis can give you a sense of the distribution of the data, and whether there are outliers and/or missing values. Most importantly, it can inform how to build your model. There are many approaches to exploratory data analysis; here are three:\n",
    "\n",
    "1. Most fundamentally: just looking at the raw values, in a spreadsheet for example. While this may seem trivial, many people ignore this crucial step!\n",
    "1. Computing summary statistics like means, medians, and standard deviations.\n",
    "1. Creating data visualizations.\n",
    "\n",
    "Let's load the data, `select` only a subset of the variables, and look at the raw values. Recall you can look at the raw values by running `View()` in the console in RStudio to pop-up the spreadsheet viewer with the data frame of interest as the argument to `View()`. Here, however, we present only a snapshot of five randomly chosen rows:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "evals = pd.read_csv(\"https://www.openintro.org/stat/data/evals.csv\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "evals_ch6 = evals[['score', 'bty_avg', 'age']]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "evals_ch6.sample(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While a full description of each of these variables can be found at [openintro.org](https://www.openintro.org/stat/data/?data=evals), let's summarize what each of these variables represents.\n",
    "\n",
    "1. `score`: Numerical variable of the average teaching score based on students' evaluations between 1 and 5. This is the outcome variable $y$ of interest.\n",
    "1. `bty_avg`: Numerical variable of average \"beauty\" rating based on a panel of 6 students' scores between 1 and 10. This is the numerical explanatory variable $x$ of interest. Here 1 corresponds to a low beauty rating and 10 to a high beauty rating.\n",
    "1. `age`: A numerical variable of age in years as an integer value.\n",
    "\n",
    "Another way to look at the raw values is using the `glimpse()` function, which gives us a slightly different view of the data. We see `Observations: 463`, indicating that there are 463 observations in `evals`, each corresponding to a particular instructor at UT Austin. Expressed differently, each row in the data frame `evals` corresponds to one of 463 instructors. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "evals_ch6.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since both the outcome variable `score` and the explanatory variable `bty_avg` are numerical, we can compute summary statistics about them such as the mean, median, and standard deviation. Let’s take `evals_ch6` and select only the two variables of interest for now. However, let's instead use the `describe()` function from the `skimr` package. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "evals_ch6[['score','bty_avg']].describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case for our numerical variables `bty_avg` beauty score and teaching score `score` it returns:\n",
    "\n",
    "- `count`: the total number of values\n",
    "- `mean`: the average\n",
    "- `std`: the standard deviation\n",
    "- `min`: the minimum: the value at which no observations are smaller than it. \n",
    "- `p25`: the 25^th^ percentile: the value at which 25% of observations are smaller than it. This is also known as the *1^st^ quartile*\n",
    "- `p50`: the 50^th^ percentile: the value at which 50% of observations are smaller than it. This is also know as the *2^nd^* quartile and more commonly the *median*\n",
    "- `p75`: the 75^th^ percentile: the value at which 75% of observations are smaller than it. This is also known as the *3^rd^ quartile*\n",
    "- `max`: the maximum: the value at which all of observations are smaller than it. \n",
    "\n",
    "We get an idea of how the values in both variables are distributed. For example, the mean teaching score was 4.17 out of 5 whereas the mean beauty score was 4.42 out of 10. Furthermore, the middle 50% of teaching scores were between 3.80 and 4.6 (the first and third quartiles) while the middle 50% of beauty scores were between 3.17 and 5.5 out of 10. \n",
    "\n",
    "The `describe()` function however only returns what are called *univariate* summaries, i.e. summaries about single variables at a time. Since we are considering the relationship between two numerical variables, it would be nice to have a summary statistic that simultaneously considers both variables. The *correlation coefficient* is a *bivariate* summary statistic that fits this bill. *Coefficients* in general are quantitative expressions of a specific property of a phenomenon. A correlation coefficient is a quantitative expression between -1 and 1 that summarizes the *strength of the linear relationship between two numerical variables*:\n",
    "\n",
    "* -1 indicates a perfect *negative relationship*: as the value of one variable goes up, the value of the other variable tends to go down.\n",
    "* 0 indicates no relationship: the values of both variables go up/down independently of each other.\n",
    "* +1 indicates a perfect *positive relationship*: as the value of one variable goes up, the value of the other variable tends to go up as well.\n",
    "\n",
    "Figure \\@ref(fig:correlation1) gives examples of different correlation coefficient values for hypothetical numerical variables $x$ and $y$. We see that while for a correlation coefficient of -0.75 there is still a negative relationship between $x$ and $y$, it is not as strong as the negative relationship between $x$ and $y$ when the correlation coefficient is -1. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "correlation = [-0.9999, -0.9, -0.75, -0.3, 0, 0.3, 0.75, 0.9, 0.9999]\n",
    "n_sim = 100\n",
    "\n",
    "values = pd.DataFrame({'v1': [], 'v2': [], 'correlation':[]})\n",
    "for i in correlation:\n",
    "    rho = i\n",
    "    sigma = sp.array( [5, rho * math.sqrt(50), rho * math.sqrt(50), 10]).reshape((2,2))\n",
    "    #print(sigma)\n",
    "    sim = sp.stats.multivariate_normal.rvs(\n",
    "        mean=[20,40], \n",
    "        cov=[[1, rho], [rho,1]], \n",
    "        size=n_sim)\n",
    "    \n",
    "    #print(sim)\n",
    "    simdf = pd.DataFrame({'v1' : sim[:,0],\n",
    "                          'v2' : sim[:,1]})\n",
    "    simdf['correlation'] = round(rho, 2)\n",
    "    values = values.append(simdf)\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(pn.ggplot(data = values, mapping = pn.aes('v1', 'v2')) +\n",
    "    pn.geom_point() +\n",
    "    pn.facet_wrap('~ correlation') +\n",
    "    pn.labs(x = \"x\", y = \"y\"))# + \n",
    "    #pn.theme(\n",
    "    #  axis.text.x = element_blank(),\n",
    "    #  axis.text.y = element_blank(),\n",
    "    #  axis.ticks = element_blank()\n",
    "    #)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The correlation coefficient can also be computed using the `cor()` function, where in this case the inputs to the function are the two numerical variables from which we want to calculate the correlation coefficient. Recall from Subsection \\@ref(exploredataframes) that specific variables from a data frame are pulled out by using square brackets:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sp.corrcoef(evals_ch6['bty_avg'], evals_ch6['score'])[0,1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In our case, the correlation coefficient of 0.187 indicates that the relationship between teaching evaluation score and beauty average is \"weakly positive.\" There is a certain amount of subjectivity in interpreting correlation coefficients, especially those that aren't close to -1, 0, and 1. For help developing such intuition and more discussion on the correlation coefficient see Subsection \\@ref(correlationcoefficient) below. \n",
    "\n",
    "Let's now proceed by visualizing this data. Since both the `score` and `bty_avg` variables are numerical, a scatterplot is an appropriate graph to visualize this data. Let's do this using `geom_point()` and set informative axes labels and title and display the result in Figure \\@ref(fig:numxplot1)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(pn.ggplot(evals_ch6, pn.aes(x = 'bty_avg', y = 'score')) +\n",
    "  pn.geom_point() +\n",
    "  pn.labs(x = \"Beauty Score\", y = \"Teaching Score\", \n",
    "       title = \"Relationship of teaching and beauty scores\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Observe the following:\n",
    "\n",
    "1. Most \"beauty\" scores lie between 2 and 8.\n",
    "1. Most teaching scores lie between 3 and 5.\n",
    "1. Recall our earlier computation of the correlation coefficient, which describes the strength of the linear relationship between two numerical variables. Looking at Figure \\@ref(fig:numxplot2), it is not immediately apparent that these two variables are positively related. This is to be expected given the positive, but rather weak (close to 0), correlation coefficient of `r cor(evals_ch6$score, evals_ch6$bty_avg) %>% round(3)`.\n",
    "\n",
    "Before we continue, we bring to light an important fact about this dataset: it suffers from *overplotting*. Recall from the data visualization Subsection \\@ref(overplotting) that overplotting occurs when several points are stacked directly on top of each other thereby obscuring the number of points. For example, let's focus on the 6 points in the top-right of the plot with a beauty score of around 8 out of 10: are there truly only 6 points, or are there many more just stacked on top of each other? You can think of these as *ties*. Let's break up these ties with a little random \"jitter\" added to the points in Figure \\@ref(fig:numxplot2). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sp.random.seed(76)\n",
    "(pn.ggplot(evals_ch6, pn.aes(x = 'bty_avg', y = 'score')) +\n",
    "  pn.geom_jitter() +\n",
    "  pn.labs(x = \"Beauty Score\", y = \"Teaching Score\", \n",
    "       title = \"Relationship of teaching and beauty scores\"))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Jittering adds a little random bump to each of the points to break up these ties: just enough so you can distinguish them, but not so much that the plot is overly altered. Furthermore, jittering is strictly a visualization tool; it does not alter the original values in the dataset. \n",
    "\n",
    "Let's compare side-by-side the regular scatterplot in Figure \\@ref(fig:numxplot1) with the jittered scatterplot in Figure \\@ref(fig:numxplot2) in Figure \\@ref(fig:numxplot2-a)."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "fig, axarr = plt.subplots(2, 1, figsize=(12, 8))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "box = pd.DataFrame({'x' : [7.6, 8, 8, 7.6, 7.6], \n",
    "                    'y' : [4.75, 4.75, 5.1, 5.1, 4.75]})\n",
    "p1 = (pn.ggplot(evals_ch6, pn.aes(x = 'bty_avg', y = 'score')) +\n",
    "  pn.geom_point() +\n",
    "  pn.labs(x = \"Beauty Score\", y = \"Teaching Score\", \n",
    "       title = \"Regular scatterplot\") +\n",
    "  pn.geom_path(mapping=pn.aes(x='x', y='y'), data=box, color = \"orange\", size = 1))\n",
    "p1\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "sp.random.seed(76)\n",
    "p2 = (pn.ggplot(evals_ch6, pn.aes(x = 'bty_avg', y = 'score')) +\n",
    "  pn.geom_jitter() +\n",
    "  pn.labs(x = \"Beauty Score\", y = \"Teaching Score\", \n",
    "       title = \"Jittered scatterplot\") +\n",
    "  pn.geom_path(mapping=pn.aes(x='x', y='y'), data=box, color = \"orange\", size = 1))\n",
    "p2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We make several further observations:\n",
    "\n",
    "<!-- We might want to actually highlight these points in the plot. -->\n",
    "\n",
    "1. Focusing our attention on the top-right of the plot again, as noted earlier where there seemed to only be 6 points in the regular scatterplot, we see there were in fact really 9 as seen in the jittered scatterplot. \n",
    "1. A further interesting trend is that the jittering revealed a large number of instructors with beauty scores of between 3 and 4.5, towards the lower end of the beauty scale. \n",
    "\n",
    "To keep things simple in this chapter, we'll present regular scatterplots rather than the jittered scatterplots, though we'll  keep the overplotting in mind whenever looking at such plots. Going back to scatterplot in Figure \\@ref(fig:numxplot1), let's improve on it by adding a \"regression line\" in Figure \\@ref(fig:numxplot3). This is easily done by adding a new layer to the `ggplot` code that created Figure \\@ref(fig:numxplot2): `+ geom_smooth(method = \"lm\")`. A regression line is a \"best fitting\" line in that of all possible lines you could draw on this plot, it is \"best\" in terms of some mathematical criteria. We discuss the criteria for \"best\" in Subsection \\@ref(leastsquares) below, but we suggest you read this only after covering the concept of a *residual* coming up in Subsection \\@ref(model1points). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(pn.ggplot(evals_ch6, pn.aes(x = 'bty_avg', y = 'score')) +\n",
    "  pn.geom_point() +\n",
    "  pn.labs(x = \"Beauty Score\", y = \"Teaching Score\", \n",
    "       title = \"Relationship of teaching and beauty scores\") +\n",
    "  pn.geom_smooth(method=\"lm\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When viewed on this plot, the regression line is a visual summary of the relationship between two numerical variables, in our case the outcome variable `score` and the explanatory variable `bty_avg`. The positive slope of the blue line is consistent with our observed correlation coefficient of `r cor(evals_ch6$score, evals_ch6$bty_avg) %>% round(3)` suggesting that there is a positive relationship between `score` and `bty_avg`. We'll see later however that while the correlation coefficient is not equal to the slope of this line, they always have the same sign: positive or negative. \n",
    "\n",
    "What are the grey bands surrounding the blue line? These are *standard error* bands, which can be thought of as error/uncertainty bands. Let's skip this idea for now and suppress these grey bars by adding the argument `se = FALSE` to `geom_smooth(method = \"lm\")`. We'll introduce standard errors in Chapter \\@ref(sampling) on sampling, use them for constructing *confidence intervals* and conducting *hypothesis tests* in Chapters \\@ref(confidence-intervals) and \\@ref(hypothesis-testing), and consider them when we revisit regression in Chapter \\@ref(inference-for-regression)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(pn.ggplot(evals_ch6, pn.aes(x = 'bty_avg', y = 'score')) +\n",
    "  pn.geom_point() +\n",
    "  pn.labs(x = \"Beauty Score\", y = \"Teaching Score\", \n",
    "       title = \"Relationship of teaching and beauty scores\") +\n",
    "  pn.geom_smooth(method=\"lm\", se=False))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_Learning check_**\n",
    "\n",
    "- Conduct a new exploratory data analysis with the same outcome variable $y$ being `score` but with `age` as the new explanatory variable $x$. Remember, this involves three things:\n",
    "\n",
    "a.  Looking at the raw values.\n",
    "b.  Computing summary statistics of the variables of interest.\n",
    "c.  Creating informative visualizations.\n",
    "\n",
    "What can you say about the relationship between age and teaching scores based on this exploration?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Simple linear regression {#model1table}\n",
    "\n",
    "You may recall from secondary school / high school algebra, in general, the equation of a line is $y = a + bx$, which is defined by two coefficients. Recall we defined this earlier as \"quantitative expressions of a specific property of a phenomenon.\" These two coefficients are:\n",
    "\n",
    "* the intercept coefficient $a$, or the value of $y$ when $x = 0$, and \n",
    "* the slope coefficient $b$, or the increase in $y$ for every increase of one in $x$.\n",
    "\n",
    "However, when defining a line specifically for regression, like the blue regression line in Figure \\@ref(fig:numxplot4), we use slightly different notation: the equation of the regression line is $\\widehat{y} = b_0 + b_1 \\cdot x$ where\n",
    "\n",
    "* the intercept coefficient is $b_0$, or the value of $\\widehat{y}$ when $x=0$, and \n",
    "* the slope coefficient $b_1$, or the increase in $\\widehat{y}$ for every increase of one in $x$.\n",
    "\n",
    "Why do we put a \"hat\" on top of the $y$? It's a form of notation commonly used in regression, which we'll introduce in the next Subsection \\@ref(model1points) when we discuss *fitted values*. For now, let's ignore the hat and treat the equation of the line as you would from secondary school / high school algebra recognizing the slope and the intercept. We know looking at Figure \\@ref(fig:numxplot4) that the slope coefficient corresponding to `bty_avg` should be positive. Why? Because as `bty_avg` increases, professors tend to roughly have larger teaching evaluation `scores`. However, what are the specific values of the intercept and slope coefficients? Let's not worry about computing these by hand, but instead let the computer do the work for us. Specifically let's use R!\n",
    "\n",
    "Let's get the value of the intercept and slope coefficients by outputting something called the *linear regression table*. We will fit the linear regression model to the `data` using the `lm()` function that comes with statsmodels and save this to `score_model`. Note that we are using the function api that was loaded with the `import statsmodels.formula.api as smf` line at the top of this file.  `ols` stands for \"ordinary least squares\", given that we are dealing with lines. When we say \"fit\", we are saying find the best fitting line to this data.\n",
    "\n",
    "The `ols()` function that \"fits\" the linear regression model is typically used as `smf.ols(formula = 'y ~ x', data = data_frame_name)` where:\n",
    "\n",
    "* `y` is the outcome variable, followed by a tilde (`~`). This is likely the key to the left of \"1\" on your keyboard. In our case, `y` is set to `score`.\n",
    "* `x` is the explanatory variable. In our case, `x` is set to `bty_avg`. We call the combination `y ~ x` a *model formula*. Recall the use of this notation when we computed the correlation coefficient using the `get_correlation()` function in Subsection \\@ref(model1EDA).\n",
    "* `data_frame_name` is the name of the data frame that contains the variables `y` and `x`. In our case, `data_frame_name` is the `evals_ch6` data frame.\n",
    "\n",
    "Note:  This use of 'statsmodels' uses R-style formulas, which are available starting in statsmodels version 0.5.0. For a more complete tutorial, you can go to the [Fitting models using R-style formulas](https://www.statsmodels.org/dev/example_formulas.html \"Fitting models using R-style formulas\") page or to the [https://patsy.readthedocs.io/en/latest/](https://patsy.readthedocs.io/en/latest/ \"patsy - Describing statistical models in Python\") reference page."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "score_model = smf.ols(formula='score ~ bty_avg', data = evals_ch6)\n",
    "score_ols = score_model.fit()\n",
    "print(score_ols.summary())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This output is telling us that the `Intercept` coefficient $b_0$ of the regression line is 3.8803 and the slope coefficient for `by_avg` is 0.0666.  Therefore the blue regression line in Figure \\@ref(fig:numxplot4) is \n",
    "\n",
    "$$\\widehat{\\text{score}} = b_0 + b_{\\text{bty avg}} \\cdot\\text{bty avg} = 3.8803 + 0.0666\\cdot\\text{ bty avg}$$ \n",
    "\n",
    "where\n",
    "\n",
    "* The intercept coefficient $b_0 = 3.8803$ means for instructors that had a hypothetical beauty score of 0, we would expect them to have on average a teaching score of 3.8803. In this case however, while the intercept has a mathematical interpretation when defining the regression line, there is no *practical* interpretation since `score` is an average of a panel of 6 students' ratings from 1 to 10, a `bty_avg` of 0 would be impossible. Furthermore, no instructors had a beauty score anywhere near 0 in this data.\n",
    "* Of more interest is the slope coefficient associated with `bty_avg`: $b_{\\text{bty avg}} = +0.0666$. This is a numerical quantity that summarizes the relationship between the outcome and explanatory variables. Note that the sign is positive, suggesting a positive relationship between beauty scores and teaching scores, meaning as beauty scores go up, so also do teaching scores go up. The slope's precise interpretation is:\n",
    "\n",
    "    > For every increase of 1 unit in `bty_avg`, there is an *associated* increase of, *on average*, 0.0666 units of `score`. \n",
    "\n",
    "Such interpretations need be carefully worded:\n",
    "\n",
    "* We only stated that there is an *associated* increase, and not necessarily a *causal* increase. For example, perhaps it's not that beauty directly affects teaching scores, but instead individuals from wealthier backgrounds tend to have had better education and training, and hence have higher teaching scores, but these same individuals also have higher beauty scores. Avoiding such reasoning can be summarized by the adage \"correlation is not necessarily causation.\" In other words, just because two variables are correlated, it doesn't mean one directly causes the other. We discuss these ideas more in Subsection \\@ref(correlation-is-not-causation).  \n",
    "* We say that this associated increase is *on average* 0.0666 units of teaching `score` and not that the associated increase is *exactly* 0.0666 units of `score` across all values of `bty_avg`. This is because the slope is the average increase across all points as shown by the regression line in Figure \\@ref(fig:numxplot4). \n",
    "\n",
    "Now that we've learned how to compute the equation for the blue regression line in Figure \\@ref(fig:numxplot4) and interpreted all its terms, let's take our modeling one step further. This time after fitting the model using the `lm()`, let's get something called the *regression table* using the `get_regression_table()` function from the `moderndive` package:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(score_ols.summary2())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we suggested in Subsection \\@ref(model1EDA), interpreting coefficients that are not close to the extreme values of -1 and 1 can be subjective. To develop your sense of correlation coefficients, we suggest you play the following 80's-style video game called \"Guess the correlation\"! Click on the image below to do so:\n",
    "\n",
    "![](notebooks/images/guess_the_correlation.png \"Guess the correlation\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Correlation is not necessarily causation {#correlation-is-not-causation}\n",
    "\n",
    "You'll note throughout this chapter we've been very cautious in making statements of the \"associated effect\" of explanatory variables on the outcome variables, for example our statement from Subsection \\@ref(model1table) that \"for every increase of 1 unit in `bty_avg`, there is an *associated* increase of, *on average*, `r evals_line[2]` units of `score`.\" We stay this because we are careful not to make *causal* statements. So while beauty score `bty_avg` is positively correlated with teaching `score`, does it directly cause effects on teaching score.\n",
    "\n",
    "For example, let's say an instructor has their `bty_avg` reevaluated, but only after taking steps to try to boost their beauty score. Does this mean that they will suddenly be a better instructor? Or will they suddenly get higher teaching scores? Maybe? \n",
    "\n",
    "Here is another example, a not-so-great medical doctor goes through their medical records and finds that patients who slept with their shoes on tended to wake up more with headaches. So this doctor declares \"Sleeping with shoes on cause headaches!\" \n",
    "\n",
    "![](notebooks/images/flowcharts/flowchart.010-cropped.png \"Does sleeping with shoes on cause headaches?\")\n",
    "\n",
    "However as some of you might have guessed, if someone is sleeping with their shoes on its probably because they are intoxicated. Furthermore, drinking more tends to cause more hangovers, and hence more headaches. \n",
    "\n",
    "In this instance, alcohol is what's known as a *confounding/lurking* variable. It \"lurks\" behind the scenes, confounding or making less apparent, the causal effect (if any) of \"sleeping with shoes on\" with waking up with a headache. We can summarize this notion in Figure \\@ref(fig:moderndive-figure-causal-graph) with a *causal graph* where:\n",
    "\n",
    "* Y: Is an *outcome* variable, here \"waking up with a headache.\"\n",
    "* X: Is a *treatment* variable whose causal effect we are interested in, here \"sleeping with shoes on.\"\n",
    "\n",
    "![](notebooks/images/flowcharts/flowchart.009-cropped.png \"Causal graph.\")\n",
    "\n",
    "So for example, many such studies use regression modeling where the outcome variable is set to Y and the explanatory/predictor variable is X, much as you've started learning how to do in this chapter. However, Figure \\@ref(fig:moderndive-figure-causal-graph) also includes a third variable with arrows pointing at both X and Y.\n",
    "\n",
    "* Z: Is a *confounding* variable that affects both X & Y, thus \"confounding\" their relationship.\n",
    "\n",
    "So as we said, alcohol will both cause people to be more likely to sleep with their shoes on as well as more likely to wake up with a headache. Thus when evaluating what causes one to wake up with a headache, its hard to tease out the effect of sleeping with shoes on versus just the alcohol. Thus our model needs to also use Z as an explanatory/predictor variable as well, in other words our doctor needs to take into account who had been drinking the night before. We'll start covering multiple regression models that allows us to incorporate more than one variable in the next chapter.\n",
    "\n",
    "Establishing causation is a tricky problem and frequently takes either carefully designed experiments or methods to control for the effects of potential confounding variables.  Both these approaches attempt either to remove all confounding variables or take them into account as best they can, and only focus on the behavior of an outcome variable in the presence of the levels of the other variable(s). Be careful as you read studies to make sure that the writers aren't falling into this fallacy of correlation implying causation.  If you spot one, you may want to send them a link to [Spurious Correlations](http://www.tylervigen.com/spurious-correlations)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Best fitting line {#leastsquares}\n",
    "\n",
    "Regression lines are also known as \"best fitting lines\". But what do we mean by best? Let's unpack the criteria\n",
    "that is used by regression to determine best. Recall the plot in Figure \\@ref(fig:numxplot5) where for a instructor\n",
    "with a beauty average score of $x=7.333$\n",
    "\n",
    "* The observed value $y=4.9$ was marked with a red circle\n",
    "* The fitted value $\\widehat{y} = 4.369$ on the regression line was marked with a red square\n",
    "* The residual $y-\\widehat{y} = 4.9-4.369 = 0.531$ was the length of the blue arrow.\n",
    "\n",
    "Let's do this for another arbitrarily chosen instructor whose beauty score was\n",
    "$x=2.333$. The residual in this case is $2.7 - 4.036 = -1.336$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "index = evals_ch6.query('bty_avg == 2.333 & score == 2.7')\n",
    "target_point = smf.ols\n",
    "x = target_point['bty_avg']\n",
    "y = target_point['score']\n",
    "y_hat = target_point['score_hat']\n",
    "resid = target_point['residual']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
