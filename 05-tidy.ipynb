{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Data Importing & \"Tidy\" Data {#tidy}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "chap = 5\n",
    "lc = 0\n",
    "rq = 0\n",
    "print(\"(LC\", chap, \".\", (lc), \")\")\n",
    "print(\"(RQ\", chap, \".\", (rq), \")\")\n",
    "lc+= 1\n",
    "rq += 1\n",
    "import scipy as sp  \n",
    "import pandas as pd  \n",
    "import plotnine as pn \n",
    "import statsmodels.api as sm\n",
    "sp.random.seed(76)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In Subsection \\@ref(programming-concepts) we introduced the concept of a data frame: a rectangular spreadsheet-like representation of data in R where the rows correspond to observations and the columns correspond to variables describing each observation.  In Section \\@ref(nycflights13), we started exploring our first data frame: the `flights` data frame included in the `nycflights13` package. In Chapter \\@ref(viz) we created visualizations based on the data included in `flights` and other data frames such as `weather`. In Chapter \\@ref(wrangling), we learned how to wrangle data, in other words take existing data frames and transform/ modify them to suit our analysis goals. \n",
    "\n",
    "In this final chapter of the \"Data Science via the tidyverse\" portion of the book, we extend some of these ideas by discussing a type of data formatting called \"tidy\" data. You will see that having data stored in \"tidy\" format is about more than what the colloquial definition of the term \"tidy\" might suggest: having your data \"neatly organized.\" Instead, we define the term \"tidy\" in a more rigorous fashion, outlining a set of rules by which data can be stored, and the implications of these rules for analyses.\n",
    "\n",
    "Although knowledge of this type of data formatting was not necessary for our treatment of data visualization in Chapter \\@ref(viz) and data wrangling in Chapter \\@ref(wrangling) since all the data was already in \"tidy\" format, we'll now see this format is actually essential to using the tools we covered in these two chapters. Furthermore, it will also be useful for all subsequent chapters in this book when we cover regression and statistical inference.  First however, we'll show you how to import spreadsheet data for use in Python.\n",
    "\n",
    "### Needed packages {-}\n",
    "\n",
    "Let's load all the packages needed for this chapter (this assumes you've already installed them). If needed, read Section \\@ref(packages) for information on how to install and load R packages."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Needed packages {-}\n",
    "\n",
    "Let's load all the packages needed for this chapter (this assumes you've already installed them). If needed, read Section \\@ref(packages) for information on how to install and load Python libraries."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Importing data {#csv}\n",
    "\n",
    "Say you have your own data saved on your computer or somewhere online? How can you analyze this data in Python? Spreadsheet data is often saved in one of the following formats:\n",
    "\n",
    "* A *Comma Separated Values* `.csv` file.  You can think of a `.csv` file as a bare-bones spreadsheet where:\n",
    "    + Each line in the file corresponds to one row of data/one observation.\n",
    "    + Values for each line are separated with commas. In other words, the values of different variables are separated by commas.\n",
    "    + The first line is often, but not always, a *header* row indicating the names of the columns/variables.\n",
    "* An Excel `.xlsx` file. This format is based on Microsoft's proprietary Excel software. As opposed to a bare-bones `.csv` files, `.xlsx` Excel files contain a lot of meta-data, or put more simply, data about the data. (Recall we saw a previous example of meta-data in Section \\@ref(groupby) when adding \"group structure\" meta-data to a data frame by using the `group_by()` verb.) Some examples of spreadsheet meta-data include the use of bold and italic fonts, colored cells, different column widths, and formula macros.\n",
    "* A [Google Sheets](https://www.google.com/sheets/about/) file, which is a \"cloud\" or online-based way to work with a spreadsheet. Google Sheets allows you to download your data in both comma separated values `.csv` and Excel `.xlsx` formats however: go to the Google Sheets menu bar -> File -> Download as -> Select \"Microsoft Excel\" or \"Comma-separated values.\"\n",
    "\n",
    "We'll cover two methods for importing `.csv` and `.xlsx` spreadsheet data in Python.\n",
    "\n",
    "### Using Pandas\n",
    "\n",
    "First, let's import a Comma Separated Values `.csv` file of data directly off the internet. The `.csv` file `dem_score.csv` accessible at <https://moderndive.com/data/dem_score.csv> contains ratings of the level of democracy in different countries spanning 1952 to 1992. Let's use the `read_csv()` function from the `pandas` to read it off the web, import it into Python, and save it in a data frame called `dem_score`.  Next, we will use `read_csv()` to read the data off of a local directory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dem_score = pd.read_csv(\"https://moderndive.com/data/dem_score.csv\")\n",
    "dem_score.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dem_score =pd. read_csv(\"notebooks/data/dem_score.csv\")\n",
    "dem_score.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this `dem_score` data frame, the minimum value of `-10` corresponds to a highly autocratic nation whereas a value of `10` corresponds to a highly democratic nation.  We'll revisit the `dem_score` data frame in a case study in the upcoming Section \\@ref(case-study-tidy).\n",
    "\n",
    "Note that the `read_csv()` function included in the `pandas` package is different than the `csv.reader()` function that is part of the Python standard library.  The pandas version is more useful in data analysis as it reads the data into a pandas dataframe. In addition, it is faster as the `csv.reader()` function reads in data one line at a time."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tidy data {#tidy-data-ex}\n",
    "\n",
    "Let's now switch gears and learn about the concept of \"tidy\" data format by starting with a motivating example. Let's consider the `drinks` data frame included in the `fivethirtyeight` data. Run the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "drinks =pd. read_csv(\"https://raw.githubusercontent.com/fivethirtyeight/data/master/alcohol-consumption/drinks.csv\")\n",
    "drinks.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After reading the help file by running `?drinks`, we see that `drinks` is a data frame containing results from a survey of the average number of servings of beer, spirits, and wine consumed for 193 countries. This data was originally reported on the data journalism website FiveThirtyEight.com in Mona Chalabi's article [\"Dear Mona Followup: Where Do People Drink The Most Beer, Wine And Spirits?\"](https://fivethirtyeight.com/features/dear-mona-followup-where-do-people-drink-the-most-beer-wine-and-spirits/)\n",
    "\n",
    "Let's apply some of the data wrangling verbs we learned in Chapter \\@ref(wrangling) on the `drinks` data frame. Let's\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's apply some of the data wrangling verbs we learned in Chapter \\@ref(wrangling) on the `drinks` data frame. Let's\n",
    "\n",
    "1. `query()` the `drinks` data frame to only consider 4 countries (the United States, China, Italy, and Saudi Arabia) then\n",
    "1. `select` all columns except `total_litres_of_pure_alcohol` by using `-` sign, then\n",
    "1. `rename()` the variables `beer_servings`, `spirit_servings`, and `wine_servings` to `beer`, `spirit`, and `wine` respectively\n",
    "\n",
    "and save the resulting data frame in `drinks_smaller`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "drinks.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "drinks_smaller = drinks.query('country in [\"USA\", \"China\", \"Italy\", \"Saudi Arabia\"]').drop(columns=['total_litres_of_pure_alcohol']).rename(columns={'beer_servings' : 'beer', 'spirit_servings' : 'spirit', 'wine_servings' : 'wine'})\n",
    "drinks_smaller"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using the `drinks_smaller` data frame, how would we create the side-by-side AKA dodged barplot in Figure \\@ref(fig:drinks-smaller)? Recall we saw barplots displaying two categorical variables in Section \\@ref(two-categ-barplot)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "drinks_smaller_tidy = drinks_smaller.melt(id_vars='country', value_vars=['beer', 'spirit', 'wine'], \n",
    "                                          var_name='type', value_name='servings')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(pn.ggplot(drinks_smaller_tidy, pn.aes(x='country', y='servings', fill='type')) +\n",
    "  pn.geom_col(position = \"dodge\") +\n",
    "  pn.labs(x = \"country\", y = \"servings\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's break down the Grammar of Graphics:\n",
    "\n",
    "1. The categorical variable `country` with four levels (China, Italy, Saudi Arabia, USA) would have to be mapped to the `x`-position of the bars.\n",
    "1. The numerical variable `servings` would have to be mapped to the `y`-position of the bars, in other words the height of the bars.\n",
    "1. The categorical variable `type` with three levels (beer, spirit, wine) who have to be mapped to the `fill` color of the bars.\n",
    "\n",
    "Observe however that `drinks_smaller` has *three separate variables* for `beer`, `spirit`, and `wine`, whereas in order to recreate the side-by-side AKA dodged barplot in Figure \\@ref(fig:drinks-smaller) we would need a *single variable* `type` with three possible values: `beer`, `spirit`, and `wine`, which we would then map to the `fill` aesthetic.  In other words, for us to be able to create the barplot in Figure \\@ref(fig:drinks-smaller), our data frame would have to look like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "drinks_smaller_tidy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "Let's compare the `drinks_smaller_tidy` with the `drinks_smaller` data frame from earlier:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "drinks_smaller"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Observe that while `drinks_smaller` and `drinks_smaller_tidy` are both rectangular in shape and contain the same 12 numerical values (3 alcohol types $\\times$ 4 countries), they are formatted differently. `drinks_smaller` is formatted in what's known as [\"wide\"](https://en.wikipedia.org/wiki/Wide_and_narrow_data) format, whereas `drinks_smaller_tidy` is formatted in what's known as [\"long/narrow\"](https://en.wikipedia.org/wiki/Wide_and_narrow_data#Narrow). In the context of using R, long/narrow format is also known as \"tidy\" format. Furthermore, in order to use the `ggplot2` and `dplyr` packages for data visualization and data wrangling, your input data frames *must* be in \"tidy\" format. So all non-\"tidy\" data must be converted to \"tidy\" format first. \n",
    "\n",
    "Before we show you how to convert non-\"tidy\" data frames like `drinks_smaller` to \"tidy\" data frames like `drinks_smaller_tidy`, let's go over the explicit definition of \"tidy\" data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Definition of \"tidy\" data\n",
    "\n",
    "You have surely heard the word \"tidy\" in your life:\n",
    "\n",
    "* \"Tidy up your room!\"\n",
    "* \"Please write your homework in a tidy way so that it is easier to grade and to provide feedback.\"\n",
    "* Marie Kondo's best-selling book [_The Life-Changing Magic of Tidying Up: The Japanese Art of Decluttering and Organizing_](https://www.amazon.com/Life-Changing-Magic-Tidying-Decluttering-Organizing/dp/1607747308/ref=sr_1_1?ie=UTF8&qid=1469400636&sr=8-1&keywords=tidying+up) and Netflix TV series [_Tidying Up with Marie Kondo_](https://www.netflix.com/title/80209379).\n",
    "* \"I am not by any stretch of the imagination a tidy person, and the piles of unread books on the coffee table and by my bed have a plaintive, pleading quality to me - 'Read me, please!'\" - Linda Grant\n",
    "\n",
    "What does it mean for your data to be \"tidy\"? While \"tidy\" has a clear English meaning of \"organized\", \"tidy\" in the context of data science using R means that your data follows a standardized format. We will follow Hadley Wickham's definition of *tidy data* here [@tidy]:\n",
    "\n",
    "> A dataset is a collection of values, usually either numbers (if quantitative)\n",
    "or strings AKA text data (if qualitative). Values are organised in two ways.\n",
    "Every value belongs to a variable and an observation. A variable contains all\n",
    "values that measure the same underlying attribute (like height, temperature,\n",
    "duration) across units. An observation contains all values measured on the same\n",
    "unit (like a person, or a day, or a city) across attributes.\n",
    "\n",
    "> Tidy data is a standard way of mapping the meaning of a dataset to its\n",
    "structure. A dataset is messy or tidy depending on how rows, columns and tables\n",
    "are matched up with observations, variables and types. In *tidy data*:\n",
    "\n",
    "> 1. Each variable forms a column.\n",
    "> 2. Each observation forms a row.\n",
    "> 3. Each type of observational unit forms a table.\n",
    "\n",
    "![http://r4ds.had.co.nz/tidy-data.html](notebooks/images/tidy-1.png \"Tidy data graphic from [R for Data Science]\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For example, say you have the following table of stock prices in Table \\@ref(tab:non-tidy-stocks):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stocks = pd.DataFrame(\n",
    "  {'Date' : pd.date_range('2009-01-01', periods=5, freq='D'),\n",
    "  'Boeing' : [\"173.55\", \"172.61\", \"173.86\", \"170.77\", \"174.29\"],\n",
    "  'Amazon' : [\"174.90\", \"171.42\", \"171.58\", \"173.89\", \"170.16\"],\n",
    "  'Google' : [\"174.34\", \"170.04\", \"173.65\", \"174.87\", \"172.19\"]}\n",
    ")\n",
    "stocks"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Although the data are neatly organized in a rectangular spreadsheet-type format, they are not in tidy format because while there are three variables corresponding to three unique pieces of information (Date, Stock Name, and Stock Price), there are not three columns. In \"tidy\" data format each variable should be its own column, as shown in Table \\@ref(tab:tidy-stocks). Notice that both tables present the same information, but in different formats. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stocks_tidy = stocks.melt(id_vars='Date', value_vars=['Boeing', 'Amazon', 'Google'], \n",
    "                          var_name='Stock Name', value_name='Strock Price')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stocks_tidy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have the requisite three columns Date, Stock Name, and Stock Price. On the other hand, consider the data in  Table \\@ref(tab:tidy-stocks-2)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stocks.weather = pd.DataFrame(\n",
    "  {'Date' : pd.date_range('2009-01-01', periods=5, freq='D'),\n",
    "  'Boeing' : [\"173.55\", \"172.61\", \"173.86\", \"170.77\", \"174.29\"],\n",
    "  'Weather' : [\"Sunny\", \"Overcast\", \"Rain\", \"Rain\", \"Sunny\"]}\n",
    ")\n",
    "stocks.weather"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, even though the variable \"Boeing\" occurs just like in our non-\"tidy\" data in Table \\@ref(tab:non-tidy-stocks), the data *is* \"tidy\" since there are three variables corresponding to three unique pieces of information: Date, Boeing stock price, and the weather that particular day."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_Learning check_**\n",
    "\n",
    "- What are common characteristics of \"tidy\" data frames?\n",
    "\n",
    "- What makes \"tidy\" data frames useful for organizing data?\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Converting to \"tidy\" data\n",
    "\n",
    "In this book so far, you've only seen data frames that were already in \"tidy\" format. Furthermore for the rest of this book, you'll mostly only see data frames that are already in \"tidy\" format as well. This is not always the case however with data in the wild. If your original data frame is in wide i.e. non-\"tidy\" format and you would like to use the `ggplot2` package for data visualization or the `dplyr` package for data wrangling, you will first have to convert it \"tidy\" format using the `gather()` function in the `tidyr` package [@R-tidyr]. \n",
    "\n",
    "Going back to our `drinks_smaller` data frame from earlier:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "drinks_smaller"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We convert it to \"tidy\" format by using the `melt()` function from the `pandas` package as follows:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "drinks_smaller_tidy = drinks_smaller.melt(id_vars='country', value_vars=['beer', 'spirit', 'wine'], \n",
    "                                          var_name='type', value_name='servings')\n",
    "drinks_smaller_tidy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We set the arguments to `melt()` as follows:\n",
    "\n",
    "1. The first argument are the columns you don't want to tidy. Observe how we set `id_vars=country` indicating that we don't want to tidy the `country` variable in `drinks_smaller` and rather only `beer`, `spirit`, and `wine`.\n",
    "1. `var_name` is the name of the column/variable in the new \"tidy\" frame that contains the column names of the original data frame that you want to tidy. Observe how we set `var_name = 'type'` and in the resulting `drinks_smaller_tidy` the column `type` contains the three types of alcohol `beer`, `spirit`, and `wine`.\n",
    "1. `value_name` is the name of the column/variable in the \"tidy\" frame that contains the rows and columns of values in the original data frame you want to tidy. Observe how we set `value_name = 'servings'` and in the resulting `drinks_smaller_tidy` the column `servings` contains the 4 $\\times$ 3 = 12 numerical values.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Converting \"wide\" format data to \"tidy\" format often confuses new Python users. The only way to learn to get comfortable with the `melt()` function is with practice, practice, and more practice. For example, see the examples in the bottom of the help file for `melt()` by running `?pd.melt`. We'll show another example of using `melt()` to convert a \"wide\" formatted data frame to \"tidy\" format in Section \\@ref(case-study-tidy). For other examples of converting a dataset into \"tidy\" format, check out the different functions available for data tidying and a case study using data from the World Health Organization in [R for Data Science](http://r4ds.had.co.nz/tidy-data.html) [@rds2016].\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_Learning check_**\n",
    "\n",
    "Take a look the `airline_safety` data frame included in the `fivethirtyeight` data. Run the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "airline_safety = pd.read_csv(\"https://raw.githubusercontent.com/fivethirtyeight/data/master/airline-safety/airline-safety.csv\")\n",
    "airline_safety.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After reading the description at https://github.com/fivethirtyeight/data/tree/master/airline-safety, we see that `airline_safety` is a data frame containing information on different airlines companies' safety records. This data was originally reported on the data journalism website FiveThirtyEight.com in Nate Silver's article [\"Should Travelers Avoid Flying Airlines That Have Had Crashes in the Past?\"](https://fivethirtyeight.com/features/should-travelers-avoid-flying-airlines-that-have-had-crashes-in-the-past/). Let's ignore the `avail_seat_km_per_week` variable for simplicity:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "airline_safety_smaller = airline_safety.drop(columns=['avail_seat_km_per_week'])\n",
    "airline_safety_smaller.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This data frame is not in \"tidy\" format. How would you convert this data frame to be in \"tidy\" format, in particular so that it has a variable `incident_type_years` indicating the incident type/year and a variable `count` of the counts?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### `nycflights13` package\n",
    "\n",
    "Recall the `nycflights13` package with data about all domestic flights departing from New York City in 2013 that we introduced in Section \\@ref(nycflights13) and used extensively in Chapter \\@ref(viz) on data visualization and Chapter \\@ref(wrangling) on data wrangling. Let's revisit the `flights` data frame by running `View(flights)`. We saw that `flights` has a rectangular shape with each of its `r scales::comma(nrow(flights))` rows corresponding to a flight and each of its `r ncol(flights)` columns corresponding to different characteristics/measurements of each flight. This matches exactly with our definition of \"tidy\" data from above.\n",
    "\n",
    "1. Each variable forms a column.\n",
    "2. Each observation forms a row.\n",
    "\n",
    "But what about the third property of \"tidy\" data?\n",
    "\n",
    "> 3. Each type of observational unit forms a table.\n",
    "\n",
    "Recall that we also saw in Section \\@ref(exploredataframes) that the observational unit for the `flights` data frame is an individual flight. In other words, the rows of the `flights` data frame refer to characteristics/measurements of individual flights. Also included in the `nycflights13` package are other data frames with their rows representing different observational units [@R-nycflights13]:\n",
    "\n",
    "* `airlines`: translation between two letter IATA carrier codes and names (`r nrow(nycflights13::airlines)` in total). i.e. the observational unit is an airline company.\n",
    "* `planes`: construction information about each of `r scales::comma(nrow(nycflights13::planes))` planes used. i.e. the observational unit is an aircraft.\n",
    "* `weather`: hourly meteorological data (about `r nycflights13::weather %>% count(origin) %>% .[[\"n\"]] %>% mean() %>% round()` observations) for each of the three NYC airports. i.e. the observational unit is an hourly measurement.\n",
    "* `airports`: airport names and locations.  i.e. the observational unit is an airport.\n",
    "\n",
    "The organization of the information into these five data frames follow the third \"tidy\" data property: observations corresponding to the same observational unit should be saved in the same table i.e. data frame. You could think of this property as the old English expression: \"birds of a feather flock together.\" "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Case study: Democracy in Guatemala {#case-study-tidy}\n",
    "\n",
    "In this section, we'll show you another example of how to convert a data frame that isn't in \"tidy\" format i.e. \"wide\" format, to a data frame that is in \"tidy\" format i.e. \"long/narrow\" format. We'll do this using the `gather()` function from the `tidyr` package again. Furthermore, we'll make use of some of the `ggplot2` data visualization and `dplyr` data wrangling tools you learned in Chapters \\@ref(viz) and \\@ref(wrangling).\n",
    "\n",
    "Let's use the `dem_score` data frame we imported in Section \\@ref(csv), but focus on only data corresponding to Guatemala."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "guat_dem = dem_score.query('country == \"Guatemala\"')\n",
    "guat_dem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's produce a *time-series plot* showing how the democracy scores have changed over the 40 years from 1952 to 1992 for Guatemala. Recall that we saw time-series plot in Section \\@ref(linegraphs) on creating linegraphs using `geom_line()`. Let's lay out the Grammar of Graphics we saw in Section \\@ref(grammarofgraphics). \n",
    "\n",
    "First we know we need to set `data = guat_dem` and use a `geom_line()` layer, but what is the aesthetic mapping of variables. We'd like to see how the democracy score has changed over the years, so we need to map:\n",
    "\n",
    "* `year` to the x-position aesthetic and\n",
    "* `democracy_score` to the y-position aesthetic\n",
    "\n",
    "Now we are stuck in a predicament, much like with our `drinks_smaller` example in Section \\@ref(tidy-data-ex). We see that we have a variable named `country`, but its only value is `\"Guatemala\"`.  We have other variables denoted by different year values.  Unfortunately, the `guat_dem` data frame is not \"tidy\" and hence is not in the appropriate format to apply the Grammar of Graphics and thus we cannot use the `ggplot2` package.  We need to take the values of the columns corresponding to years in `guat_dem` and convert them into a new \"key\" variable called `year`. Furthermore, we'd like to take the democracy scores on the inside of the table and turn them into a new \"value\" variable called `democracy_score`.  Our resulting data frame will thus have three columns:  `country`, `year`, and `democracy_score`. \n",
    "\n",
    "Recall that the `melt()` function in the `pandas` can complete this task for us:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "guat_dem_tidy = guat_dem.melt(id_vars='country',  value_vars=['1952', '1957', '1962', '1967', '1972', '1977', '1982', '1987', '1992'],\n",
    "                              var_name='year', value_name = 'democracy_score') \n",
    "guat_dem_tidy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "guat_dem_tidy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We set the arguments to `gather()` as follows:\n",
    "\n",
    "1. The third argument are the columns you don't want to tidy. Observe how we set  `id_vars='country'` indicating that we don't want to tidy the `country` variable in `guat_dem` and rather only `1952` through `1992`. \n",
    "1. `var_name` are the name of the column/variable in the new \"tidy\" frame that contains the column names of the original data frame that you want to tidy. Observe how we set `var_name = 'year'` and in the resulting `guat_dem_tidy` the column `year` contains the years where the Guatemala's democracy score were measured.\n",
    "1. `value_name` is the name of the column/variable in the \"tidy\" frame that contains the rows and columns of values in the original data frame you want to tidy. Observe how we set `value_name = 'democracy_score'` and in the resulting `guat_dem_tidy` the column `democracy_score` contains the 1 $\\times$ 9 = 9 democracy scores.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "guat_dem_tidy.year"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "However, observe in the output for `guat_dem_tidy` that the `year` variable is of type `object`. Before we can plot this variable on the x-axis, we need to convert it into a numerical variable using the `as.numeric()` function within the `assign()` function, which we saw in Section \\@ref(mutate) on mutating existing variables to create new ones."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "guat_dem_tidy['year'] = guat_dem_tidy['year'].astype(int)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "guat_dem_tidy.year"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now create the plot to show how the democracy score of Guatemala changed from 1952 to 1992 using a `geom_line()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(pn.ggplot(guat_dem_tidy, pn.aes(x = 'year', y = 'democracy_score')) +\n",
    "  pn.geom_line() +\n",
    "  pn.labs(x = \"Year\", y = \"Democracy Score\", title = \"Democracy score in Guatemala 1952-1992\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**_Learning check_**\n",
    "\n",
    "-  Convert the `dem_score` data frame into a tidy data frame and assign the name of `dem_score_tidy` to the resulting long-formatted data frame.\n",
    "\n",
    "-  Read in the life expectancy data stored at <https://moderndive.com/data/le_mess.csv> and convert it to a tidy data frame. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion\n",
    "\n",
    "### `tidyverse` package\n",
    "\n",
    "Notice at the beginning of the chapter we loaded the following libraries, which are among the most frequently used Python libraries for data science:\n",
    "\n",
    "```\n",
    "import scipy as sp  \n",
    "import pandas as pd  \n",
    "import plotnine as pn \n",
    "```\n",
    "\n",
    "\n",
    "### Additional resources\n",
    "\n",
    "\n",
    "If you want to learn more about using the `pandas` library, we suggest you that you check out Data Wrangling with Pandas cheatsheet. You can access this cheatsheet by going to the Pandas website (https://pandas.pydata.org/Pandas_Cheat_Sheet.pdf) and searching for \"Pandas Cheat Sheet\".\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
