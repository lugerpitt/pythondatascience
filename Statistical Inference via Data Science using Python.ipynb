{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Statistical Inference via Data Science\n",
    "## Using Python with pandas, statmodels, and plotnine\n",
    "### Louis Luangkesorn\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import scipy as sp  \n",
    "import pandas as pd  \n",
    "import sklearn as sk  \n",
    "import matplotlib.pyplot as plt \n",
    "import plotnine as pn \n",
    "import statsmodels as sm\n",
    "from IPython.display import Image"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    " import sys\n",
    " print(sys.executable)\n",
    " print(sys.version)\n",
    " print(sys.version_info)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import plotnine"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction for students {#sec:intro-for-students}\n",
    "\n",
    "This book assumes no prerequisites: no algebra, no calculus, and no prior programming/coding experience. This is intended to be a gentle introduction to the practice of analyzing data and answering questions using data the way data scientists, statisticians, data journalists, and other researchers would. \n",
    "\n",
    "In Figure \\@ref(fig:moderndive-figure) we present a flowchart of what you'll cover in this book. You'll first get started with data in Chapter \\@ref(getting-started), where you'll learn about the relative roles between Python and the Spyder IDE, start coding in Python, understand what Python libraries are, and explore your first dataset: all domestic departure flights from a New York City airport in 2013. Then\n",
    "\n",
    "1. **Data science**: You'll assemble your data science toolbox using the `scipy` and related libraries In particular:\n",
    "    + Ch.\\@ref(viz): Visualizing data via the `plotnine` library.\n",
    "    + Ch.\\@ref(tidy): Understanding the concept of \"tidy\" data as a standardized data input format for all packages in the `tidyverse`  **check this to see if it is better to say pandas**\n",
    "    + Ch.\\@ref(wrangling): Wrangling data via the `pandas` library.\n",
    "1. **Data modeling**: Using these data science tools and helper functions from the `statsmodels` library, you'll start performing data modeling. In particular:\n",
    "    + Ch.\\@ref(regression): Constructing basic regression models.\n",
    "    + Ch.\\@ref(multiple-regression): Constructing multiple regression models.\n",
    "1. **Statistical inference**: Once again using your newly acquired data science tools, we'll unpack statistical inference using the `infer` package. In particular:  **also currently scipy Look at Think Stats for better way of saying this**\n",
    "    + Ch.\\@ref(sampling): Understanding the role that sampling variability plays in statistical inference using both tactile and virtual simulations of sampling from a \"bowl\" with an unknown proportion of red balls.\n",
    "    + Ch.\\@ref(confidence-intervals): Building confidence intervals.\n",
    "    + Ch.\\@ref(hypothesis-testing): Conducting hypothesis tests.\n",
    "1. **Data modeling revisited**: Armed with your new understanding of statistical inference, you'll revisit and review the models you constructed in Ch.\\@ref(regression) & Ch.\\@ref(multiple-regression). In particular:\n",
    "    + Ch.\\@ref(inference-for-regression): Interpreting both the statistical and practice significance of the results of the models.\n",
    "\n",
    "We'll end with a discussion on what it means to \"think with data\" in Chapter \\@ref(thinking-with-data) and present an example case study data analysis of house prices in Seattle.\n",
    "\n",
    "\n",
    "![Data science workflow](images/flowcharts/flowchart/flowchart.002.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "\n",
    "### What you will learn from this book {#subsec:learning-goals}\n",
    "\n",
    "We hope that by the end of this book, you'll have learned\n",
    "\n",
    "1. How to use Python to explore data.  \n",
    "1. How to answer statistical questions using tools like confidence intervals and hypothesis tests. \n",
    "1. How to effectively create \"data stories\" using these tools. \n",
    "\n",
    "What do we mean by data stories? We mean any analysis involving data that engages the reader in answering questions with careful visuals and thoughtful discussion, such as [How strong is the relationship between per capita income and crime in Chicago neighborhoods?](http://rpubs.com/ry_lisa_elana/chicago) and [How many f**ks does Quentin Tarantino give (as measured by the amount of swearing in his films)?](https://ismayc.github.io/soc301_s2017/group_projects/group4.html).  Further discussions on data stories can be found in this [Think With Google article](https://www.thinkwithgoogle.com/marketing-resources/data-measurement/tell-meaningful-stories-with-data/).  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This book will help you develop your \"data science toolbox\", including tools such as data visualization, data formatting, data wrangling, and data modeling using regression. With these tools, you'll be able to perform the entirety of the \"data/science pipeline\" while building data communication skills (see Subsection \\@ref(subsec:pipeline) for more details). \n",
    "\n",
    "In particular, this book will lean heavily on data visualization.  In today's world, we are bombarded with graphics that attempt to convey ideas.  We will explore what makes a good graphic and what the standard ways are to convey relationships with data.  You'll also see the use of visualization to introduce concepts like mean, median, standard deviation, distributions, etc.  In general, we'll use visualization as a way of building almost all of the ideas in this book.\n",
    "\n",
    "To impart the statistical lessons in this book, we have intentionally minimized the number of mathematical formulas used and instead have focused on developing a conceptual understanding via data visualization, statistical computing, and simulations.  We hope this is a more intuitive experience than the way statistics has traditionally been taught in the past and how it is commonly perceived.\n",
    "\n",
    "Finally, you'll learn the importance of literate programming.  By this we mean you'll learn how to write code that is useful not just for a computer to execute but also for readers to understand exactly what your analysis is doing and how you did it.  This is part of a greater effort to encourage reproducible research (see Subsection \\@ref(subsec:reproducible) for more details). Hal Abelson coined the phrase that we will follow throughout this book:\n",
    "\n",
    "> \"Programs must be written for people to read, and only incidentally for machines to execute.\"\n",
    "\n",
    "We understand that there may be challenging moments as you learn to program.  Both of us continue to struggle and find ourselves often using web searches to find answers and reach out to colleagues for help.  In the long run though, we all can solve problems faster and more elegantly via programming.  We wrote this book as our way to help you get started and you should know that there is a huge community of Python users that are always happy to help everyone along as well.  This community exists in particular on the internet on various forums and websites such as [stackoverflow.com](https://stackoverflow.com/).\n",
    "\n",
    "### Data/science pipeline {#subsec:pipeline}\n",
    "\n",
    "You may think of statistics as just being a bunch of numbers.  We commonly hear the phrase \"statistician\" when listening to broadcasts of sporting events.  Statistics (in particular, data analysis), in addition to describing numbers like with baseball batting averages, plays a vital role in all of the sciences.  You'll commonly hear the phrase \"statistically significant\" thrown around in the media.  You'll see articles that say \"Science now shows that chocolate is good for you.\"  Underpinning these claims is data analysis.  By the end of this book, you'll be able to better understand whether these claims should be trusted or whether we should be wary.  Inside data analysis are many sub-fields that we will discuss throughout this book (though not necessarily in this order):\n",
    "\n",
    "- data collection\n",
    "- data wrangling\n",
    "- data visualization\n",
    "- data modeling\n",
    "- inference\n",
    "- correlation and regression\n",
    "- interpretation of results\n",
    "- data communication/storytelling\n",
    "\n",
    "These sub-fields are summarized in what Grolemund and Wickham term the [\"Data/Science Pipeline\"](http://r4ds.had.co.nz/explore-intro.html) in Figure \\@ref(fig:pipeline-figure).\n",
    "\n",
    "```{r pipeline-figure, echo=FALSE, fig.align='center', fig.cap=\"Data/Science Pipeline\"}\n",
    "knitr::include_graphics(\"images/tidy1.png\")\n",
    "```\n",
    "\n",
    "We will begin by digging into the gray **Understand** portion of the cycle with data visualization, then with a discussion on what is meant by tidy data and data wrangling, and then conclude by talking about interpreting and discussing the results of our models via **Communication**.  These steps are vital to any statistical analysis.  But why should you care about statistics?  \"Why did they make me take this class?\"\n",
    "\n",
    "There's a reason so many fields require a statistics course. Scientific knowledge grows through an understanding of statistical significance and data analysis. You needn't be intimidated by statistics.  It's not the beast that it used to be and, paired with computation, you'll see how reproducible research in the sciences particularly increases scientific knowledge.\n",
    "\n",
    "### Reproducible research {#subsec:reproducible}\n",
    "\n",
    "> \"The most important tool is the _mindset_, when starting, that the end product will be reproducible.\" – Keith Baggerly\n",
    "\n",
    "Another goal of this book is to help readers understand the importance of reproducible analyses. The hope is to get readers into the habit of making their analyses reproducible from the very beginning.  This means we'll be trying to help you build new habits.  This will take practice and be difficult at times. You'll see just why it is so important for you to keep track of your code and well-document it to help yourself later and any potential collaborators as well.  \n",
    "\n",
    "Copying and pasting results from one program into a word processor is not the way that efficient and effective scientific research is conducted.  It's much more important for time to be spent on data collection and data analysis and not on copying and pasting plots back and forth across a variety of programs.\n",
    "\n",
    "In a traditional analyses if an error was made with the original data, we'd need to step through the entire process again:  recreate the plots and copy and paste all of the new plots and our statistical analysis into your document.  This is error prone and a frustrating use of time.  We'll see how to use R Markdown to get away from this tedious activity so that we can spend more time doing science.\n",
    "\n",
    "> \"We are talking about _computational_ reproducibility.\" - Yihui Xie\n",
    "\n",
    "Reproducibility means a lot of things in terms of different scientific fields.  Are experiments conducted in a way that another researcher could follow the steps and get similar results?  In this book, we will focus on what is known as **computational reproducibility**.  This refers to being able to pass all of one's data analysis, data-sets, and conclusions to someone else and have them get exactly the same results on their machine.  This allows for time to be spent interpreting results and considering assumptions instead of the more error prone way of starting from scratch or following a list of steps that may be different from machine to machine.\n",
    "\n",
    "<!--\n",
    "Additionally, this book will focus on computational thinking, data thinking, and inferential thinking. We'll see throughout the book how these three modes of thinking can build effective ways to work with, to describe, and to convey statistical knowledge.  \n",
    "-->\n",
    "\n",
    "### Final note for students\n",
    "\n",
    "At this point, if you are interested in instructor perspectives on this book, ways to contribute and collaborate, or the technical details of this book's construction and publishing, then continue with the rest of the chapter below.  Otherwise, let's get started with Python and the Spyder IDE in Chapter \\@ref(getting-started)!\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## Introduction for instructors {#sec:intro-instructors}\n",
    "\n",
    "This book is inspired by the following books:\n",
    "\n",
    "- \"Mathematical Statistics with Resampling and R\" [@hester2011],\n",
    "- \"OpenIntro: Intro Stat with Randomization and Simulation\" [@isrs2014], and \n",
    "- \"R for Data Science\" [@rds2016].\n",
    "- \"ModernDive: Introdution to Statistical Analysis and Data Science using R\" [@moderndive2019]\n",
    "\n",
    "\n",
    "The first book, while designed for upper-level undergraduates and graduate students, provides an excellent resource on how to use resampling to impart statistical concepts like sampling distributions using computation instead of large-sample approximations and other mathematical formulas.  The last two books are free options to learning introductory statistics and data science, providing an alternative to the many traditionally expensive introductory statistics textbooks. \n",
    "\n",
    "When looking over the large number of introductory statistics textbooks that currently exist, we found that few directly incorporated implementation code directly into the text, and the ones that did were not in Python (generally, they used R). From the point of view of Python texts, most introductory Python texts were directed towards a beginners in object oriented programming and the Python data analysis texts were oriented towards those who already had a background in Python. Additionally, there wasn't an open-source and easily reproducible textbook available that exposed new learners all of three of the learning goals listed at the outset of Subsection \\@ref(subsec:learning-goals).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Who is this book for?\n",
    "\n",
    "This book is intended for instructors of traditional introductory data analysis classes using Python, either the desktop or server version, who would like to inject more data science topics into their syllabus. We assume that students taking the class will have no prior algebra, calculus, nor programming/coding experience.\n",
    "\n",
    "Here are some principles and beliefs we kept in mind while writing this text. If you agree with them, this might be the book for you.\n",
    "\n",
    "1. **Blur the lines between lecture and lab**\n",
    "    + With increased availability and accessibility of laptops and open-source non-proprietary statistical software, the strict dichotomy between lab and lecture can be loosened.\n",
    "    + It's much harder for students to understand the importance of using software if they only use it once a week or less.  They forget the syntax in much the same way someone learning a foreign language forgets the rules. Frequent reinforcement is key.\n",
    "1. **Focus on the entire data/science research pipeline**\n",
    "    + We believe that the entirety of Grolemund and Wickham's [data/science pipeline](http://r4ds.had.co.nz/introduction.html) should be taught.\n",
    "    + We believe in [\"minimizing prerequisites to research\"](https://arxiv.org/abs/1507.05346): students should be answering questions with data as soon as possible.\n",
    "1. **It's all about the data**\n",
    "    + We leverage Python libraries and other publicly available data for rich, real, and realistic data-sets that at the same time are easy-to-load into Python, such as the `nycflights13` and `fivethirtyeight` packages.\n",
    "    + We believe that [data visualization is a gateway drug for statistics](http://escholarship.org/uc/item/84v3774z) and that the Grammar of Graphics as implemented in the `plotnine` package is the best way to impart such lessons. However, we often hear: \"You can't teach `plotnine` for data visualization in intro stats!\" We, like [David Robinson](http://varianceexplained.org/r/teach_ggplot2_to_beginners/), are much more optimistic.\n",
    "    + `pandas` with its implementation of the data manipulation verbs can make data wrangling much more [accessible](http://chance.amstat.org/2015/04/setting-the-stage/) to novices, and hence much more interesting data-sets can be explored. \n",
    "1. **Use simulation/resampling to introduce statistical inference, not probability/mathematical formulas**\n",
    "    + Instead of using formulas, large-sample approximations, and probability tables, we teach statistical concepts using resampling-based inference.\n",
    "    + This allows for a de-emphasis of traditional probability topics, freeing up room in the syllabus for other topics.\n",
    "1. **Don't fence off students from the computation pool, throw them in!**\n",
    "    + Computing skills are essential to working with data in the 21st century. Given this fact, we feel that to shield students from computing is to ultimately do them a disservice.\n",
    "    + We are not teaching a course on coding/programming per se, but rather just enough of the computational and algorithmic thinking necessary for data analysis.\n",
    "1. **Complete reproducibility and customizability**\n",
    "    + We are frustrated when textbooks give examples, but not the source code and the data itself. We give you the source code for all examples as well as the whole book!\n",
    "    + Ultimately the best textbook is one you've written yourself. You know best your audience, their background, and their priorities. You know best your own style and the types of examples and problems you like best. Customization is the ultimate end. For more about how to make this book your own, see [About this Book](#sec:about-book).\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# ACKNOWLEDGEMENTS\n",
    "\n",
    "**NEEDS REPLACING**\n",
    "\n",
    "* Please let us know if you find any errors, typos, or areas from improvement on our [GitHub issues](https://github.com/moderndive/moderndive_book/issues) page.\n",
    "* If you are familiar with GitHub and would like to contribute more, please see Section \\@ref(sec:about-book) below.\n",
    "\n",
    "\n",
    "\n",
    "For example, we thank\n",
    "\n",
    "* [Dr Andrew Heiss](https://twitter.com/andrewheiss) for contributing Subsection 2.2.3 on \"Errors, warnings, and messages\".\n",
    "\n",
    "The authors would like to thank [Nina Sonneborn](https://github.com/nsonneborn), [Kristin Bott](https://twitter.com/rhobott?lang=en), [Dr. Jenny Smetzer](https://www.smith.edu/academics/faculty/jennifer-smetzer), and the participants of our [USCOTS 2017 workshop](https://www.causeweb.org/cause/uscots/uscots17/workshop/3) for their feedback and suggestions.  A special thanks goes to Dr. Yana Weinstein, cognitive psychological scientist and co-founder of [The Learning Scientists](http://www.learningscientists.org/yana-weinstein/), for her extensive contributions.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
